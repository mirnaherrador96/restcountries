function APICountries(info){
let url = `https://restcountries.eu/rest/v2/${info}`;
const app = new XMLHttpRequest();
app.open('GET', url, true);
app.send();
app.onreadystatechange = function (){
if(this.status == 200 && this.readyState  == 4){
    let inf = JSON.parse(this.responseText);
    console.log(inf);
    let datosAPI = document.querySelector('#datosAPI');
    datosAPI.innerHTML = '';
    for(let item of inf){
        console.log(item.name);
        datosAPI.innerHTML += `
            <div class="card mb-4" style="max-width: 450px; margin-left:10px; margin-top:20px;" >
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="${item.flag}" class="card-img mt-5 ml-2" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold text-uppercase">${item.name}</h5>
                            <hr>
                            <h6 class="card-text">Capital: ${item.capital}</h6>
                            <h6 class="card-text">Región: ${item.region}</h6>
                            <h6 class="card-text">Sub-Región: ${item.subregion}</h6>
                            <small class="text-muted">Población: ${item.population}</small>
                        </div>
                    </div>
                </div>
            </div>`;
            }
        }
    }
}
function validar(){
    if ($('#buscar').text() == "") {
        APICountries('all');
        return false;
    }
};